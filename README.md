# README

The aim of this project is to have something using Zurb Foundation using webpack in Rails 6.

The Gemfile and the package.json are the first elements to
check for customisation.

## Project creation
First, I have checked the currently installaled version of Ruby (2.7.0) and Rails (6.0.3.1)

Then I have created the project:

```bash
rails new test-foundation --webpack=coffee
```

and now, the fun starts!

## Before installing Foundation

Most Rails 6/Foundation examples I have seen cope with the CSS part of Foundation.  I want to use the SASS (scss/sass) part of Foundation.  So, I need to add the sass/scss loader and configure the webpacker engine to use SASS to generate CSS from loaded .scss or .sass files. 

So, let's add the appropriate loaders:

```bash
yarn add css-loader sass-loader
```

Once installed, I need to install them.

This is done in config/webpack/environment.js

First we add references to files that we will create shortly:

```js
const sass = require('./loaders/sass')
const scss = require('./loaders/scss')

```

and then, we append these loaders to the environment:

```js
environment.loaders.prepend('sass', sass)
environment.loaders.prepend('scss', scss)
```

Then, we create the two files:

/config/webpack/loaders/sass.js and
/config/webpack/loaders/scss.js

Now, I will start the project to check if nothing crashes so far!!!

##Adding Foundation

Now, we will use yarn once again to import Foundation, Motion-UI and why not, JQuery

```bash
yarn add jquery foundation-sites motion-ui
```

NOTE: Missing loader here: add style-loader + sass

At this point, the package.json file starts to be heavy:

```json
{
  "name": "test_foundation",
  "private": true,
  "dependencies": {
    "@rails/actioncable": "^6.0.0",
    "@rails/activestorage": "^6.0.0",
    "@rails/ujs": "^6.0.0",
    "@rails/webpacker": "4.2.2",
    "coffee-loader": "^1.0.0",
    "coffeescript": "1.12.7",
    "css-loader": "^3.5.3",
    "foundation-sites": "^6.6.3",
    "jquery": "^3.5.1",
    "motion-ui": "^2.0.3",
    "sass-loader": "^8.0.2",
    "turbolinks": "^5.2.0"
  },
  "version": "0.1.0",
  "devDependencies": {
    "webpack-dev-server": "^3.11.0"
  }
}
```

Let's move outside the config world and move in the /app/javascript section of our project.

First, you create a directory name 'foundation'. You add the files named '_settings.scss', 'application.scss' and 'index.js' in that new directory.

In app/javascript/application.js you add (on the first line):

```js
import "foundation"
```

and just to make sure, go back to app/javascript/foundation/index.js and add:

```js
console.log("Zurb Foundation loaded");
```

at the right place.

## Missing parts (to be added at the right place above)


- yarn add style-loader
- yarn add sass
- yarn add -D webpack-cli
- yarn add -D babel-loader
- yarn add -D @babel/core  @babel/plugin-proposal-class-properties @babel/plugin-syntax-dynamic-import @babel/plugin-transform-runtime @babel/preset-env


