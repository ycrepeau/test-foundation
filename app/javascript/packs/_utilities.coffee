import $ from 'jquery'
$$ = (args..., func) ->
  if args.length == 0
    target = window.document
    binding = 'turbolinks:load'
  if args.length == 1
    target = window.document
    binding = args[0]
  if args >= 2
    target = args[0]
    binding = args[1]

  target.addEventListener binding , func

$$ 'DOMContentLoaded', ->
    console.log 'utilities loaded!!'

$$ 'turbolinks:render', ->
  console.log 'turbolinks:render **'

$$ 'turbolinks:before-visit', ->
  console.log 'turbolinks:before-visit'

$$ 'turbolinks:before-render', ->
  console.log 'turbolinks:before-render'

$$ 'turbolinks:load', ->
  console.log 'turbolinks:load'
  $(document).foundation()

